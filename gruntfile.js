﻿module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);
    'use strict';

    var slnPath = 'poupe.me.sln';
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        
        nugetrestore: {
            restore: {
                src: slnPath,
                dest: 'packages/'
            }
        },

        assemblyinfo: {
            options: {
                files: [
                    'Api/Api.csproj'
                ],
                info: {
                    version: "<%= pkg.version %>.0",
                    fileVersion: "<%= pkg.version %>.0",
                    company: 'afonsof',
                    product: 'Poupe.Me',
                    copyright: 'Copyright © Poupe.Me ' + (new Date().getYear() + 1900)
                }
            }
        },

        msbuild: {
            src: [slnPath],
            options: {
                projectConfiguration: 'Release',
                targets: ['Clean', 'Rebuild'],
                stdout: true,
                version: 4.0,
                verbosity: 'minimal',
                buildParameters: {
                   // WarningLevel: 5,
                    DeployOnBuild: true,
                    PublishProfile: "temp"
                },
            }
        },

        nunit: {
            options: {
                files: ['./Tests/Tests.csproj'],
                teamcity: true
            }
        },

        compress: {
            main: {
                options: {
                    archive: 'pub/poupe.me-<%= pkg.version %>.zip',
                    level: 9
                },
                files: [
                    {
                        expand: true,
                        src: ['**/*'],
                        cwd: 'temp'
                    }
                ]
            }
        },
        
        ftpush: {
            build: {
                auth: {
                    host: 'ftp.robotomoney.kinghost.net',
                    username: "robotomoney",
                    password: grunt.option('ftppass'),
                    port: 21,
                },
                src: 'temp',
                dest: '/www/api',
                simple: true
            }
        }       
    });
    grunt.registerTask('ci', ['nugetrestore', 'assemblyinfo', 'msbuild', 'nunit']);
    grunt.registerTask('deploy', ['ci', 'ftpush']);
    
};