using JetBrains.Annotations;

namespace PoupeMe.Api.Models
{
    [UsedImplicitly]
    public class NamedEntity
    {
        public int Id { get; set; }

        [UsedImplicitly]
        public string Name { get; set; }
    }
}