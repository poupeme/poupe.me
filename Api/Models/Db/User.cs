using System;
using NHibernate.Validator.Constraints;
using NHibernateBootstrap;

namespace PoupeMe.Api.Models.Db
{
    public class User : IHaveId
    {
        public virtual int Id { get; set; }
        public virtual string Email { get; set; }
        public virtual string Password { get; set; }
        public virtual string FullName { get; set; }
        public virtual string UserName { get; set; }

        //todo: public virtual DateTime CreatedAt { get; set; }

        [Length(Int32.MaxValue)]
        public virtual string CategoryClassifier { get; set; }
    }
}