using System;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Interfaces;

namespace PoupeMe.Api.Models.Db
{
    public class BudgetItem : IHaveId, IHaveUser
    {
        public virtual int Id { get; set; }

        public virtual User User { get; set; }

        public virtual DateTime Period { get; set; }

        public virtual Dealer Category { get; set; }

        public virtual decimal Value { get; set; }

        public virtual string Memo { get; set; }
    }
}