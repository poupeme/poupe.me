using System;
using JetBrains.Annotations;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Interfaces;

namespace PoupeMe.Api.Models.Db
{
    public class Transaction : IHaveId, IHaveUser
    {
        public virtual int Id { get; set; }
        public virtual User User { get; set; }
        public virtual string Hash { get; set; }

        [UsedImplicitly]
        public virtual DateTime Date { get; set; }

        [UsedImplicitly]
        public virtual string Memo { get; set; }

        public virtual Dealer SourceDealer { get; set; }
        public virtual Dealer DestinyDealer { get; set; }

        public virtual decimal Value { get; set; }
    }
}