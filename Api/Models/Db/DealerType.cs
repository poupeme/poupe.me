namespace PoupeMe.Api.Models.Db
{
    public enum DealerType
    {
        Account = 0,
        Incoming = 1,
        Expense = 2
    }
}