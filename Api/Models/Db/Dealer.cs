using JetBrains.Annotations;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Interfaces;

namespace PoupeMe.Api.Models.Db
{
    public class Dealer : IHaveId, IHaveUser
    {
        public virtual int Id { get; set; }
        public virtual User User { get; set; }

        [UsedImplicitly]
        public virtual string Name { get; set; }

        [UsedImplicitly]
        public virtual decimal OpeningBalance { get; set; }

        [UsedImplicitly]
        public virtual DealerType Type { get; set; }

        public override string ToString()
        {
            return Type + ":" + Name;
        }
    }
}