using PoupeMe.Api.Models.Db;

namespace PoupeMe.Api.Models.Interfaces
{
    public interface IHaveUser
    {
        User User { get; set; }
    }
}