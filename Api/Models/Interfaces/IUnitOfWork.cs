using NHibernateBootstrap;
using PoupeMe.Api.Models.Db;

namespace PoupeMe.Api.Models.Interfaces
{
    public interface IUnitOfWork
    {
        User User { get; }
        IRepository<Transaction> TransactionRepository { get; }
        IRepository<Dealer> AccountRepository { get; }
        IRepository<Dealer> CategoryRepository { get; }
        IRepository<Dealer> DealerRepository { get; }
        IRepository<BudgetItem> BudgetItemRepository { get; }
        IRepository<User> UserRepository { get; }
        IExtraDbOperations Operations { get; }
        bool Login(int userId);
        bool Login(string userId, string password);
        void Dispose();
        void Commit();
    }
}