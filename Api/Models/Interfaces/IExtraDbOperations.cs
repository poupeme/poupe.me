using PoupeMe.Api.Models.Db;

namespace PoupeMe.Api.Models.Interfaces
{
    public interface IExtraDbOperations
    {
        decimal GetYearOpeningBalance(int year);
        decimal GetAccountBalance(Dealer account);
    }
}