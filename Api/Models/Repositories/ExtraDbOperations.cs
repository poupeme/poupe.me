using System;
using System.Linq;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.Interfaces;

namespace PoupeMe.Api.Models.Repositories
{
    public class ExtraDbOperations : IExtraDbOperations
    {
        private readonly UnitOfWork _db;

        public ExtraDbOperations(UnitOfWork db)
        {
            _db = db;
        }

        public decimal GetYearOpeningBalance(int year)
        {
            var openingBalance = _db.AccountRepository.AsQueryable().Sum(d => d.OpeningBalance);

            var today = DateTime.Today;
            var beginningOfLastYear = new DateTime(year, 1, 1);

            if (year <= today.Year)
            {
                var lastYearBalance = TransactionsUntilDate(beginningOfLastYear);
                return openingBalance + lastYearBalance;
            }
            var beginningOfMonth = new DateTime(today.Year, today.Month, 1);

            var transactionsUntilBeginningOfMonth = TransactionsUntilDate(beginningOfMonth);
            var budgetFromBeginningOfMonthToBegginingOfLastYear = BudgetBetweenDates(beginningOfMonth, beginningOfLastYear);

            return openingBalance + transactionsUntilBeginningOfMonth + budgetFromBeginningOfMonthToBegginingOfLastYear;
        }

        public decimal GetAccountBalance(Dealer account)
        {
            const string queryString = @"select 
                                        sum(case
                                            when t.SourceDealer.Id = :dealerId
                                                then -t.Value
                                            when t.DestinyDealer.Id = :dealerId
                                                then t.Value
                                            else 0
                                        end)
                                    from Transaction t where t.SourceDealer.Id = :dealerId or t.DestinyDealer.Id = :dealerId";
            var q = _db.Session
                .CreateQuery(queryString)
                .SetParameter("dealerId", account.Id);

            return q.UniqueResult<decimal>();
        }

        #region PrivateMethods

        private decimal BudgetBetweenDates(DateTime start, DateTime end)
        {
            const string queryString = @"select 
                                        sum(case
                                            when (b.Category.Type = 'Expense')
                                                then -b.Value
                                            when b.Category.Type = 'Incoming'
                                                then b.Value
                                            else 0
                                        end)
                                    from BudgetItem b
                                    where b.User = :user
                                    and b.Period >= :start
                                    and b.Period < :end";
            var query = _db.Session.CreateQuery(queryString);
            query.SetParameter("start", start);
            query.SetParameter("end", end);
            query.SetParameter("user", _db.User);
            var ret = query.UniqueResult<decimal>();

            ret -= _db.BudgetItemRepository
                .AsQueryable()
                .Where(b => b.User == _db.User && b.Period >= start && b.Period < end && b.Category == null)
                .Sum(b=>b.Value);

            return ret;
        }

        private decimal TransactionsUntilDate(DateTime date)
        {
            const string queryString = @"select 
                                        sum(case
                                            when t.SourceDealer.Type = 'Account'
                                                then -t.Value
                                            when t.DestinyDealer.Type = 'Account'
                                                then t.Value
                                            else 0
                                        end)
                                    from Transaction t
                                    where (t.SourceDealer.Type != 'Account' or t.DestinyDealer.Type != 'Account')
                                    and t.User = :user
                                    and t.Date < :date";
            var query = _db.Session.CreateQuery(queryString);
            query.SetParameter("date", date);
            query.SetParameter("user", _db.User);
            return query.UniqueResult<decimal>();
        }

        #endregion
    }
}