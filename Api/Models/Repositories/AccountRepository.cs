using System.Linq;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Db;

namespace PoupeMe.Api.Models.Repositories
{
    public class AccountRepository : AuthorizedRepository<Dealer>
    {
        public AccountRepository(UnitOfWorkBase db)
            : base(db)
        {
        }

        public override IQueryable<Dealer> AsQueryable()
        {
            return base.AsQueryable().Where(d => d.Type == DealerType.Account);
        }
    }
}