﻿using System.Linq;
using BayesSharp;
using NHibernateBootstrap;
using PoupeMe.Api.Components;
using PoupeMe.Api.Models.Db;

namespace PoupeMe.Api.Models.Repositories
{
    public class ClassifierRepo
    {
        private readonly IRepository<User> _userRepository;
        private readonly string _userName;

        public ClassifierRepo(IRepository<User> userRepository, string userName)
        {
            _userRepository = userRepository;
            _userName = userName;
        }

        public void Save(Transaction transaction)
        {
            int? id = null;

            if (transaction.SourceDealer.Type == DealerType.Incoming)
            {
                id = transaction.SourceDealer.Id;
            }
            else if (transaction.DestinyDealer.Type == DealerType.Expense)
            {
                id = transaction.DestinyDealer.Id;
            }
            if (id.HasValue)
            {
                var classifier = Get();
                classifier.Train(id.Value, transaction.Memo);
                Save(classifier);
            }
        }

        public void Save(BayesClassifier<string, int> classifier)
        {
            var json = classifier.ExportJsonData();
            var user = _userRepository.AsQueryable().FirstOrDefault(u => u.UserName == _userName);
            if (user != null)
            {
                user.CategoryClassifier = json;
                _userRepository.Edit(user);
                _userRepository.Commit();
            }
        }

        public BayesClassifier<string, int> Get()
        {
            var user = _userRepository.AsQueryable().FirstOrDefault(u => u.UserName == _userName);
            var classifier = new BayesClassifier<string, int>(new HashCodeTokenizer());
            if (user != null && user.CategoryClassifier != null)
            {
                classifier.ImportJsonData(user.CategoryClassifier);
            }
            return classifier;
        }
    }
}