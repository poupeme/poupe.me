using System.Linq;
using System.Net;
using System.Web.Http;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.Interfaces;

namespace PoupeMe.Api.Models.Repositories
{
    public class AuthorizedRepository<TEntity> : Repository<TEntity>, IHaveUser where TEntity : class, IHaveId, IHaveUser
    {
        private User _user;

        public User User
        {
            get
            {
                if (_user == null)
                {
                    throw new HttpResponseException(HttpStatusCode.Unauthorized);
                }
                return _user;
            }
            set { _user = value; }
        }

        public AuthorizedRepository(UnitOfWorkBase db)
            : base(db)
        {
        }

        public override void Add(TEntity obj)
        {
            obj.User = User;
            base.Add(obj);
        }

        public override void Remove(TEntity obj)
        {
            if (User == null) return;
            base.Remove(obj);
        }

        public override void Edit(TEntity obj)
        {
            obj.User = User;
            base.Edit(obj);
        }

        public override TEntity Find(int id)
        {
            return AsQueryable().FirstOrDefault(e => e.Id == id);
        }

        public override IQueryable<TEntity> AsQueryable()
        {
            return base.AsQueryable().Where(e => e.User == User);
        }
    }
}