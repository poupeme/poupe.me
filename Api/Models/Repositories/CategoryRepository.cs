using System.Linq;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Db;

namespace PoupeMe.Api.Models.Repositories
{
    public class CategoryRepository : AuthorizedRepository<Dealer>
    {
        public CategoryRepository(UnitOfWorkBase db) : base(db)
        {
        }

        public override IQueryable<Dealer> AsQueryable()
        {
            return base.AsQueryable().Where(d => d.Type != DealerType.Account);
        }
    }
}