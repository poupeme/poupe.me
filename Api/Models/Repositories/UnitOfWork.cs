using System.Linq;
using NHibernateBootstrap;
using PoupeMe.Api.Components;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.Interfaces;

namespace PoupeMe.Api.Models.Repositories
{
    public class UnitOfWork : UnitOfWorkBase, IUnitOfWork, IHaveUser
    {
        public IExtraDbOperations Operations { get; private set; }
        public User User { get; set; }

        public UnitOfWork()
        {
            Operations = new ExtraDbOperations(this);
        }

        public bool Login(string username, string password)
        {
            User = UserRepository
                .AsQueryable()
                .FirstOrDefault(u => u.Email == username && u.Password == password.ToPasswordHash());
            return Login();
        }

        public bool Login(int userId)
        {
            User = UserRepository.Find(userId);
            return Login();
        }

        private bool Login()
        {
            if (User == null) return false;
            var properties = GetType().GetProperties();
            
                //.Where(p => typeof (IHaveUser).IsAssignableFrom(p.PropertyType));

            foreach (var propertyInfo in properties)
            {
                var repository = propertyInfo.GetValue(this) as IHaveUser;
                if (repository != null) repository.User = User;
            }
            return true;
        }

        private IRepository<Transaction> _transactions;
        public IRepository<Transaction> TransactionRepository
        {
            get { return _transactions ?? (_transactions = new AuthorizedRepository<Transaction>(this)); }
        }

        private IRepository<Dealer> _accounts;
        public IRepository<Dealer> AccountRepository
        {
            get { return _accounts ?? (_accounts = new AccountRepository(this)); }
        }

        private IRepository<Dealer> _category;
        public IRepository<Dealer> CategoryRepository
        {
            get { return _category ?? (_category = new CategoryRepository(this)); }
        }

        private IRepository<Dealer> _dealer;
        public IRepository<Dealer> DealerRepository
        {
            get { return _dealer ?? (_dealer = new AuthorizedRepository<Dealer>(this)); }
        }

        private IRepository<BudgetItem> _budgetItems;
        public IRepository<BudgetItem> BudgetItemRepository
        {
            get { return _budgetItems ?? (_budgetItems = new AuthorizedRepository<BudgetItem>(this)); }
        }

        private IRepository<User> _users;
        public IRepository<User> UserRepository
        {
            get { return _users ?? (_users = new Repository<User>(this)); }
        }
    }
}