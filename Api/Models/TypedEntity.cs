using JetBrains.Annotations;

namespace PoupeMe.Api.Models
{
    public class TypedEntity : NamedEntity
    {
        [UsedImplicitly]
        public int Type { get; set; }
    }
}