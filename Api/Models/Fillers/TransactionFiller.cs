using NHibernateBootstrap;
using PoupeMe.Api.Components;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.ViewModels.Transaction;

namespace PoupeMe.Api.Models.Fillers
{
    public class TransactionFiller : IEntityFiller<TransactionVm, Transaction>
    {
        private readonly IRepository<Dealer> _dealerRepo;
        private readonly IHashCreator _hashCreator;

        public TransactionFiller(IRepository<Dealer> dealerRepo, IHashCreator hashCreator)
        {
            _dealerRepo = dealerRepo;
            _hashCreator = hashCreator;
        }

        public Transaction Fill(TransactionVm vm, Transaction obj = null)
        {
            if (obj == null)
            {
                obj = new Transaction();
            }
            obj.Date = vm.Date;
            obj.Memo = vm.Memo;
            obj.Value = vm.Value;
            obj.Id = vm.Id;
            obj.SourceDealer = vm.SourceDealerId.HasValue ? _dealerRepo.Find(vm.SourceDealerId.Value) : null;
            obj.DestinyDealer = vm.DestinyDealerId.HasValue ? _dealerRepo.Find(vm.DestinyDealerId.Value): null;
            obj.Hash = vm.Hash ?? _hashCreator.Create(vm.Date, vm.Memo, vm.Value);
            return obj;
        }
    }
}