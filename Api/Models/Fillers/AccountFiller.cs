using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.ViewModels.Account;

namespace PoupeMe.Api.Models.Fillers
{
    public class AccountFiller : IEntityFiller<AccountVm, Dealer>
    {
        public Dealer Fill(AccountVm vm, Dealer obj = null)
        {
            if (obj == null)
            {
                obj = new Dealer();
            }
            obj.Id = vm.Id;
            obj.Name = vm.Name;
            obj.Type = DealerType.Account;
            obj.OpeningBalance = vm.OpeningBalance;
            return obj;
        }
    }
}