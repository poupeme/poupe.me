using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.ViewModels.Category;

namespace PoupeMe.Api.Models.Fillers
{
    public class CategoryFiller : IEntityFiller<CategoryVm, Dealer>
    {
        public Dealer Fill(CategoryVm vm, Dealer obj = null)
        {
            if (obj == null) obj = new Dealer();

            obj.Id = vm.Id;
            obj.Name = vm.Name;
            obj.Type = vm.Type;
            return obj;
        }
    }
}