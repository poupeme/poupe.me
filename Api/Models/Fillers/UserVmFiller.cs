using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.ViewModels.User;

namespace PoupeMe.Api.Models.Fillers
{
    public class UserVmFiller : IEntityFiller<User, UserVm>
    {
        public UserVm Fill(User obj, UserVm vm = null)
        {
            if (vm == null) vm = new UserVm();
            vm.Email = obj.Email;
            vm.FullName = obj.FullName;
            vm.Id = obj.Id;
            vm.UserName = obj.UserName;
            return vm;
        }
    }
}