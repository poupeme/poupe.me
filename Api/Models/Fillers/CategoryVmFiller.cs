using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.ViewModels.Category;

namespace PoupeMe.Api.Models.Fillers
{
    public class CategoryVmFiller : IEntityFiller<Dealer, CategoryVm>
    {
        public CategoryVm Fill(Dealer obj, CategoryVm vm = null)
        {
            if (vm == null) vm = new CategoryVm();
            vm.Id = obj.Id;
            vm.Name = obj.Name;
            vm.Type = obj.Type;
            return vm;
        }
    }
}