using JetBrains.Annotations;
using NHibernateBootstrap;

namespace PoupeMe.Api.Models.Fillers
{
    public interface IEntityFiller<in TEntity1, TEntity2> where TEntity2 : class, IHaveId
    {
        TEntity2 Fill([NotNull] TEntity1 obj, TEntity2 vm = null);
    }
}