using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.Interfaces;
using PoupeMe.Api.Models.ViewModels.Account;

namespace PoupeMe.Api.Models.Fillers
{
    public class AccountVmFiller : IEntityFiller<Dealer, AccountVm>
    {
        private readonly IExtraDbOperations _dbOperations;

        public AccountVmFiller(IExtraDbOperations dbOperations)
        {
            _dbOperations = dbOperations;
        }

        public AccountVm Fill(Dealer obj, AccountVm vm = null)
        {
            if (vm == null)
            {
                vm = new AccountVm();
            }
            vm.Id = obj.Id;
            vm.Name = obj.Name;
            vm.OpeningBalance = obj.OpeningBalance;
            vm.Balance = _dbOperations.GetAccountBalance(obj);
            return vm;
        }
    }
}