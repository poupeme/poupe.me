using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.ViewModels.Transaction;

namespace PoupeMe.Api.Models.Fillers
{
    public class TransactionVmFiller : IEntityFiller<Transaction, TransactionVm>
    {
        public TransactionVm Fill(Transaction obj, TransactionVm vm = null)
        {
            if (vm == null) vm = new TransactionVm();
            vm.Id = obj.Id;
            vm.Date = obj.Date;
            vm.DestinyDealerId = obj.DestinyDealer != null ? obj.DestinyDealer.Id : (int?)null;
            vm.SourceDealerId = obj.SourceDealer != null ? obj.SourceDealer.Id : (int?)null;
            vm.Memo = obj.Memo;
            vm.Hash = obj.Hash;
            vm.Value = obj.Value;
            return vm;
        }
    }
}