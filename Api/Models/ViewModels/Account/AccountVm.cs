﻿using JetBrains.Annotations;
using NHibernateBootstrap;

namespace PoupeMe.Api.Models.ViewModels.Account
{
    public class AccountVm : IHaveId
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal OpeningBalance { get; set; }

        [UsedImplicitly]
        public decimal Balance { get; set; }
    }
}