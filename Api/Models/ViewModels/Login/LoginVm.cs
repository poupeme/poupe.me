using JetBrains.Annotations;

namespace PoupeMe.Api.Models.ViewModels.Login
{
    public class LoginVm
    {
        [UsedImplicitly]
        public string Email { get; set; }

        [UsedImplicitly]
        public string Password { get; set; }

        [UsedImplicitly]
        public string AccessToken { get; set; }
    }
}