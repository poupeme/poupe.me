﻿using JetBrains.Annotations;

namespace PoupeMe.Api.Models.ViewModels.User
{
    public class UserLoginVm
    {
        [UsedImplicitly]
        public string Email { get; set; }

        [UsedImplicitly]
        public string Password { get; set; }
    }
}