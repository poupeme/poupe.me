using JetBrains.Annotations;
using NHibernateBootstrap;

namespace PoupeMe.Api.Models.ViewModels.User
{
    public class UserVm : IHaveId
    {
        [UsedImplicitly]
        public int Id { get; set; }

        [UsedImplicitly]
        public string UserName { get; set; }

        [UsedImplicitly]
        public string FullName { get; set; }

        [UsedImplicitly]
        public string Email { get; set; }
    }
}