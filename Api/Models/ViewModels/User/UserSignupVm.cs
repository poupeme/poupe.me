﻿using JetBrains.Annotations;

namespace PoupeMe.Api.Models.ViewModels.User
{
    [UsedImplicitly]
    public class UserSignupVm
    {
        [UsedImplicitly]
        public string Email { get; set; }

        [UsedImplicitly]
        public string Password { get; set; }

        [UsedImplicitly]
        public string FullName { get; set; }

        [UsedImplicitly]
        public string UserName { get; set; }
    }
}