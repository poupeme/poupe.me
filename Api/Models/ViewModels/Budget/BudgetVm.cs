using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using PoupeMe.Api.Models.Interfaces;

namespace PoupeMe.Api.Models.ViewModels.Budget
{
    public class BudgetVm
    {
        [UsedImplicitly]
        public List<List<BudgetItemVm>> Budgets { get; set; }

        [UsedImplicitly]
        public decimal LastBalance { get; set; }

        [UsedImplicitly]
        public int Year { get; set; }

        [UsedImplicitly]
        public DateTime Today { get; set; }
    }
}
