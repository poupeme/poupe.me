using JetBrains.Annotations;

namespace PoupeMe.Api.Models.ViewModels.Budget
{
    public class BudgetItemVm
    {
        [UsedImplicitly]
        public int? CategoryId { get; set; }

        [UsedImplicitly]
        public decimal RealValue { get; set; }

        [UsedImplicitly]
        public decimal PlannedValue { get; set; }

        [UsedImplicitly]
        public string Memo { get; set; }
    }
}