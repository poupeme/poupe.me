﻿using System;
using NHibernateBootstrap;

namespace PoupeMe.Api.Models.ViewModels.Transaction
{
    
    public class TransactionVm: IHaveId
    {
        public int Id { get; set; }

        public string Hash { get; set; }

        public DateTime Date { get; set; }

        public decimal Value { get; set; }

        public int? SourceDealerId { get; set; }

        public int? DestinyDealerId { get; set; }

        public string Memo { get; set; }
    }
}