using System.Collections.Generic;
using JetBrains.Annotations;

namespace PoupeMe.Api.Models.ViewModels.Transaction
{
    public class TransactionImportListVm
    {
        [UsedImplicitly]
        public int ImportDealerId { get; set; }

        [UsedImplicitly]
        public IEnumerable<TransactionImportItemVm> Transactions { get; set; }
    }
}