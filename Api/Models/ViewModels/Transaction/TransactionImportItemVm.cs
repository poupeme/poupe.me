using System;
using JetBrains.Annotations;

namespace PoupeMe.Api.Models.ViewModels.Transaction
{
    public class TransactionImportItemVm
    {
        [UsedImplicitly]
        public string Hash { get; set; }

        [UsedImplicitly]
        public DateTime Date { get; set; }

        [UsedImplicitly]
        public string Memo { get; set; }

        public int DestinyDealerId { get; set; }

        public decimal Value { get; set; }

        [UsedImplicitly]
        public bool Duplicated { get; set; }
        public bool Selected { get; set; }
    }
}