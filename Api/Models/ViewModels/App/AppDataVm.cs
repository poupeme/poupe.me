using System.Collections.Generic;
using JetBrains.Annotations;

namespace PoupeMe.Api.Models.ViewModels.App
{
    public class AppDataVm
    {
        [UsedImplicitly]
        public IEnumerable<NamedEntity> Accounts { get; set; }

        [UsedImplicitly]
        public IEnumerable<TypedEntity> Categories { get; set; }

        [UsedImplicitly]
        public IEnumerable<NamedEntity> Dealers { get; set; }

        [UsedImplicitly]
        public IEnumerable<TypedEntity> IncomingDealers { get; set; }

        [UsedImplicitly]
        public IEnumerable<TypedEntity> ExpenseDealers { get; set; }

        [UsedImplicitly]
        public IEnumerable<NamedEntity> CategoryTypes { get; set; }

        [UsedImplicitly]
        public int CurrentMonth { get; set; }

        [UsedImplicitly]
        public int CurrentYear { get; set; }
    }
}