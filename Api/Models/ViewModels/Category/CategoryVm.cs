using NHibernateBootstrap;
using PoupeMe.Api.Models.Db;

namespace PoupeMe.Api.Models.ViewModels.Category
{
    public class CategoryVm : IHaveId
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DealerType Type { get; set; }
    }
}