﻿using System.Configuration;
using System.Web;
using System.Web.Http;
using FluentNHibernate.Cfg.Db;
using NHibernate.Dialect;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Repositories;

namespace PoupeMe.Api
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            var config = MySQLConfiguration.Standard.Dialect<MySQL5Dialect>().ConnectionString(connectionString);
            NHibernateBuilder.Setup<UnitOfWork>(config, "PoupeMe.Api.Models.Db");
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            GlobalConfiguration.Configuration.EnsureInitialized(); 
        }
    }
}
