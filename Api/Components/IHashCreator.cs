using System;

namespace PoupeMe.Api.Components
{
    public interface IHashCreator
    {
        string Create(DateTime date, string memo, decimal value);
    }
}