using System;
using System.Text;
using Dongle.Algorithms;

namespace PoupeMe.Api.Components
{
    public class HashCreator : IHashCreator
    {
        public string Create(DateTime date, string memo, decimal value)
        {
            return HumanReadableHash.Compute(date + memo.ToLowerInvariant() + value, Encoding.ASCII);
        }
    }
}