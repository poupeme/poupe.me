﻿using System;
using System.Collections.Generic;
using System.Linq;
using BayesSharp.Tokenizers;
using Dongle.System;

namespace PoupeMe.Api.Components
{
    public class HashCodeTokenizer : ITokenizer<string>
    {
        public IEnumerable<string> Tokenize(object input)
        {
            var list = new List<string>
            {
                "compra",
                "com",
                "cartão",
                "pagamento",
                "de"
            };

            var simpleTokenizer = new SimpleTextTokenizer(true, list);
            var items = simpleTokenizer.Tokenize(input);
            int output;
            return items.Where(item => !Int32.TryParse(item, out output)).Select(s => s.RemoveAccents());
        }
    }
}