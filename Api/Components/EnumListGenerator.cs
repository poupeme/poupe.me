﻿using System;
using System.Collections.Generic;
using System.Linq;
using PoupeMe.Api.Models;

namespace PoupeMe.Api.Components
{
    public static class EnumListGenerator
    {
        public static IEnumerable<NamedEntity> Generate<TClass>()
        {
            return Enum
                .GetValues(typeof(TClass))
                .Cast<int>()
                .Select(d =>
                {
                    var key = Enum.GetName(typeof(TClass), d);
                    if (key == null) return null;
                    return new NamedEntity
                    {
                        Id = d,
                        Name = key
                    };
                });
        }
    }
}