﻿using Dongle.System;

namespace PoupeMe.Api.Components
{
    public static class PasswordHash
    {
        public static string ToPasswordHash(this string password)
        {
            return ("{{ " + password + " }}").ToMd5();
        }
    }
}