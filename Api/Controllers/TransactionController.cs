﻿using System;
using System.Collections;
using System.Net.Http;
using JetBrains.Annotations;
using PoupeMe.Api.Components;
using PoupeMe.Api.Controllers.Actions;
using PoupeMe.Api.Models.Fillers;
using PoupeMe.Api.Models.Repositories;
using PoupeMe.Api.Models.ViewModels.Transaction;
using Transaction = PoupeMe.Api.Models.Db.Transaction;

namespace PoupeMe.Api.Controllers
{
    public class TransactionController : ApiControllerBase
    {
        private readonly GetAction<TransactionVm, Transaction> _getAction;
        private readonly ListAction<TransactionVm, Transaction> _listAction;
        private readonly PostAction<TransactionVm, Transaction> _postAction;
        private readonly PutAction<TransactionVm, Transaction> _putAction;
        private readonly DeleteAction<Transaction> _deleteAction;
        private readonly Lazy<ClassifierRepo> _classifierRepo;

        public  TransactionController()
        {
            _classifierRepo = new Lazy<ClassifierRepo>(() => new ClassifierRepo(Db.UserRepository, User.Identity.Name));
            var hashCreator = new HashCreator();
            var transactionFiller = new TransactionFiller(Db.DealerRepository, hashCreator);
            var transactionGetFiller = new TransactionVmFiller();

            _getAction = new GetAction<TransactionVm, Transaction>(Db.TransactionRepository, transactionGetFiller);
            _listAction = new ListAction<TransactionVm, Transaction>(Db.TransactionRepository, transactionGetFiller);
            _postAction = new PostAction<TransactionVm, Transaction>(Db.TransactionRepository, transactionFiller);
            _putAction = new PutAction<TransactionVm, Transaction>(Db.TransactionRepository, transactionFiller);
            _deleteAction = new DeleteAction<Transaction>(Db.TransactionRepository);
        }

        [UsedImplicitly]
        public IEnumerable Get(string month = null)
        {
            Func<Transaction, bool> where = null;

            if (month != null)
            {
                var splitted = month.Split('/');
                var monthNum = Convert.ToInt32(splitted[0]) - 1;
                var yearNum = Convert.ToInt32(splitted[1]);

                var initialDate = new DateTime(yearNum, monthNum, 1);
                var finalDate = initialDate.AddMonths(1).AddDays(-1);

                where = (t => t.Date >= initialDate && t.Date <= finalDate);
            }

            return _listAction.Do(where, t => t.Date);
        }

        [UsedImplicitly]
        public object Get(int id)
        {
            return _getAction.Do(id);
        }

        [UsedImplicitly]
        public HttpResponseMessage Post(TransactionVm vm)
        {
            return _postAction.Do(this, vm, _classifierRepo.Value.Save);
        }

        [UsedImplicitly]
        public HttpResponseMessage Put(int id, TransactionVm vm)
        {
            return _putAction.Do(id, vm);
        }

        [UsedImplicitly]
        public HttpResponseMessage Delete(int id)
        {
            return _deleteAction.Do(id);
        }
    }
}
