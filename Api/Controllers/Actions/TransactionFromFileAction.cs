using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dongle.System.IO;
using NHibernateBootstrap;
using OfxSharpLib;
using PoupeMe.Api.Components;
using PoupeMe.Api.Models.Repositories;
using PoupeMe.Api.Models.ViewModels.Transaction;
using Transaction = PoupeMe.Api.Models.Db.Transaction;

namespace PoupeMe.Api.Controllers.Actions
{
    public class TransactionFromFileAction
    {
        private readonly IRepository<Transaction> _repo;
        private readonly IHashCreator _hashCreator;
        private readonly Lazy<ClassifierRepo> _classifierRepo;

        public TransactionFromFileAction(IRepository<Transaction> repo, IHashCreator hashCreator, Lazy<ClassifierRepo> classifierRepo)
        {
            _repo = repo;
            _hashCreator = hashCreator;
            _classifierRepo = classifierRepo;
        }

        public Task<IEnumerable> Do(ApiController c)
        {
            // Check if the request contains multipart/form-data.
            if (!c.Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var root = HttpContext.Current.Server.MapPath("~/App_Data");
            var directory = new DirectoryInfo(root);
            directory.CreateRecursively();

            var provider = new MultipartFormDataStreamProvider(root);

            // Read the form data and return an async task.
            var task = c.Request.Content.ReadAsMultipartAsync(provider).
                ContinueWith(taskLambda =>
                {
                    if (taskLambda.IsFaulted || taskLambda.IsCanceled)
                    {
                        c.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, taskLambda.Exception);
                    }

                    var parser = new OfxDocumentParser();

                    IEnumerable transactions = new List<TransactionImportItemVm>();

                    foreach (var file in provider.FileData)
                    {
                        var ofxDocument =
                            parser.Import(new FileStream(file.LocalFileName, FileMode.Open, FileAccess.Read));

                        transactions = ofxDocument.Transactions.Select(t =>
                        {
                            var hash = _hashCreator.Create(t.Date, t.Memo, t.Amount);
                            var isDuplicated = IsDuplicated(hash);

                            var ret = new TransactionImportItemVm
                            {
                                Hash = hash,
                                Date = t.Date,
                                Memo = t.Memo,
                                Value = t.Amount,
                                Duplicated = isDuplicated,
                                Selected = !isDuplicated,
                            };
                            var tags = _classifierRepo.Value.Get().Classify(t.Memo);
                            if (tags.Count > 0 && tags.First().Value > 0.5)
                            {
                                ret.DestinyDealerId = tags.First().Key;
                            }
                            return ret;

                        });
                    }
                    return transactions;
                });
            return task;
        }

        private bool IsDuplicated(string hash)
        {
            return _repo
                .AsQueryable()
                .Any(t => t.Hash == hash);
        }
    }
}