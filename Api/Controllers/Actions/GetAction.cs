using System.Net;
using System.Web.Http;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Controllers.Actions
{
    public class GetAction<TVm, TEntity>
        where TEntity : class, IHaveId
        where TVm : class, IHaveId
    {
        private readonly IEntityFiller<TEntity, TVm> _filler;
        private readonly IRepository<TEntity> _repo;

        public GetAction(IRepository<TEntity> repo, IEntityFiller<TEntity, TVm> filler)
        {
            _repo = repo;
            _filler = filler;
        }

        public TVm Do(int id)
        {
            var transaction = _repo.Find(id);
            if (transaction == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return _filler.Fill(transaction);
        }
    }
}