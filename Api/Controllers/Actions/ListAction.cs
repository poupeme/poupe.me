using System;
using System.Collections.Generic;
using System.Linq;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Controllers.Actions
{
    public class ListAction<TVm, TEntity>
        where TEntity : IHaveId
        where TVm : class, IHaveId
    {
        private readonly IRepository<TEntity> _repo;
        private readonly IEntityFiller<TEntity, TVm> _filler;

        public ListAction(IRepository<TEntity> repo, IEntityFiller<TEntity, TVm> filler)
        {
            _repo = repo;
            _filler = filler;
        }

        public List<TVm> Do<TKey>(Func<TEntity, bool> where, Func<TEntity, TKey> order)
        {
            IEnumerable<TEntity> query = _repo.AsQueryable();

            if (where != null)
            {
                query = query.Where(where);
            }
            if (order != null)
            {
                query = query.OrderBy(order);
            }

            var sel = query.Select(t => _filler.Fill(t));
            return sel.ToList();
        }

        public List<TVm> Do<TKey>(Func<TEntity, TKey> order)
        {
            return Do(null, order);
        }
    }
}