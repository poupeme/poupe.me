﻿using System;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Controllers.Actions
{
    public class PostAction<TVm, TEntity>
        where TEntity : class, IHaveId
        where TVm : IHaveId
    {
        private readonly IEntityFiller<TVm, TEntity> _filler;
        private readonly IRepository<TEntity> _repo;

        public PostAction(IRepository<TEntity> repo, IEntityFiller<TVm, TEntity> filler)
        {
            _repo = repo;
            _filler = filler;
        }

        public HttpResponseMessage Do(ApiController c, TVm vm, Action<TEntity> postActionCallback = null)
        {
            //todo: injetar um validador if (!c.ModelState.IsValid) throw new HttpResponseException(HttpStatusCode.BadRequest);

            var obj = _filler.Fill(vm);
            _repo.Add(obj);
            _repo.Commit();

            if (postActionCallback != null)
            {
                postActionCallback.Invoke(obj);
            }

            var response = new HttpResponseMessage(HttpStatusCode.Created)
            {
                Content = new StringContent(obj.Id.ToString(CultureInfo.InvariantCulture))
            };
            return response;
        }
    }
}