﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using NHibernateBootstrap;
using PoupeMe.Api.Components;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.ViewModels.Budget;

namespace PoupeMe.Api.Controllers.Actions
{
    public class BudgetPostAction
    {
        private readonly IRepository<BudgetItem> _repo;
        private readonly IRepository<Dealer> _dealerRepo;

        public BudgetPostAction(IRepository<BudgetItem> repo, IRepository<Dealer> dealerRepo)
        {
            _repo = repo;
            _dealerRepo = dealerRepo;
        }

        public HttpResponseMessage Do(BudgetVm vm)
        {
            //todo:
            /*if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }*/
            for (var monthId = 0; monthId < Constants.MonthCount; monthId++)
            {
                var monthNumber = monthId + 1;
                var budgetMonth = vm.Budgets[monthId];
                foreach (var item in budgetMonth)
                {
                    var localItem = item;
                    var dbItem = _repo
                        .AsQueryable()
                        .FirstOrDefault(b => b.Category.Id == localItem.CategoryId && b.Period.Month == monthNumber && b.Period.Year == vm.Year);

                    if (dbItem != null)
                    {
                        if (item.PlannedValue == 0 && string.IsNullOrEmpty(item.Memo))
                        {
                            _repo.Remove(dbItem);
                        }
                        else
                        {
                            dbItem.Value = item.PlannedValue;
                            dbItem.Memo = item.Memo;
                            _repo.Edit(dbItem);
                        }
                    }
                    else if (item.PlannedValue != 0 || !string.IsNullOrEmpty(item.Memo))
                    {
                        dbItem = new BudgetItem
                        {
                            Category = item.CategoryId.HasValue ? _dealerRepo.Find(item.CategoryId.Value) : null,
                            Memo = item.Memo,
                            Period = new DateTime(vm.Year, monthNumber, 1),
                            Value = item.PlannedValue,
                        };
                        _repo.Add(dbItem);
                    }
                }
            }
            try
            {
                _repo.Commit();
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}