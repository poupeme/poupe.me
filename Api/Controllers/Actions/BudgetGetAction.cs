﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernateBootstrap;
using PoupeMe.Api.Components;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.Interfaces;
using PoupeMe.Api.Models.ViewModels.Budget;

namespace PoupeMe.Api.Controllers.Actions
{
    public class BudgetGetAction
    {
        private readonly IRepository<BudgetItem> _repo;
        private readonly IExtraDbOperations _operations;
        private readonly IRepository<Transaction> _tranRepo;
        private readonly BudgetMonthAction _monthAction;

        public BudgetGetAction(IRepository<BudgetItem> repo, IRepository<Transaction> tranRepo, BudgetMonthAction monthAction, IExtraDbOperations operations)
        {
            _repo = repo;
            _tranRepo = tranRepo;
            _operations = operations;
            _monthAction = monthAction;
        }

        public BudgetVm Do(int year)
        {
            var vm = new BudgetVm
            {
                Today = DateTime.Today,
                Year = year,
                LastBalance = _operations.GetYearOpeningBalance(year),
                Budgets = new List<List<BudgetItemVm>>()
            };

            var budgetItems = _repo.AsQueryable().Where(b => b.Period.Year == year).ToList();
            var transactions = _tranRepo.AsQueryable().Where(t => t.Date.Year == year).ToList();

            for (var monthId = 0; monthId < Constants.MonthCount; monthId++)
            {
                vm.Budgets.Add(_monthAction.Do(monthId, budgetItems, transactions));
            }
            return vm;
        }
    }
}