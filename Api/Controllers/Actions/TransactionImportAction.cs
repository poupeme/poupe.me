using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dongle.Reflection;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.Repositories;
using PoupeMe.Api.Models.ViewModels.Transaction;
using Transaction = PoupeMe.Api.Models.Db.Transaction;

namespace PoupeMe.Api.Controllers.Actions
{
    public class TransactionImportAction
    {
        private readonly IRepository<Transaction> _repo;
        private readonly IRepository<Dealer> _dealerRepo;
        private readonly Lazy<ClassifierRepo> _classifierRepo;

        public TransactionImportAction(IRepository<Transaction> repo, IRepository<Dealer> dealerRepo, Lazy<ClassifierRepo> classifierRepo)
        {
            _repo = repo;
            _dealerRepo = dealerRepo;
            _classifierRepo = classifierRepo;
        }

        public HttpResponseMessage Do(TransactionImportListVm vm)
        {
            //todo: if (!c.ModelState.IsValid) { return c.Request.CreateErrorResponse(HttpStatusCode.BadRequest, c.ModelState); }

            var classifier = _classifierRepo.Value.Get();

            foreach (var item in vm.Transactions.Where(t => t.Selected))
            {

                var transaction = new Transaction
                {
                    Date = item.Date,
                    Memo = item.Memo,
                    Hash = item.Hash
                };

                if (item.Value > 0)
                {
                    transaction.SourceDealer = _dealerRepo.Find(item.DestinyDealerId);
                    transaction.DestinyDealer = _dealerRepo.Find(vm.ImportDealerId);
                }
                else
                {
                    transaction.SourceDealer = _dealerRepo.Find(vm.ImportDealerId);
                    transaction.DestinyDealer = _dealerRepo.Find(item.DestinyDealerId);
                }
                transaction.Value = Math.Abs(transaction.Value);
                _repo.Add(transaction);

                classifier.Train(item.DestinyDealerId, transaction.Memo);
            }
            _repo.Commit();
            _classifierRepo.Value.Save(classifier);

            return new HttpResponseMessage(HttpStatusCode.Created);
        }
    }
}