﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Controllers.Actions
{
    public class PutAction<TVm, TEntity>
        where TEntity : class, IHaveId
        where TVm : IHaveId
    {
        private readonly IEntityFiller<TVm, TEntity> _filler;
        private readonly IRepository<TEntity> _repo;

        public PutAction(IRepository<TEntity> repo, IEntityFiller<TVm, TEntity> filler)
        {
            _repo = repo;
            _filler = filler;
        }

        public HttpResponseMessage Do(int id, TVm vm, Action<TEntity> postActionCallback = null)
        {
            //todo
            /*if (!c.ModelState.IsValid)
            {
                return c.Request.CreateErrorResponse(HttpStatusCode.BadRequest, c.ModelState);
            }*/

            if (id != vm.Id)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            var obj = _repo.Find(vm.Id);
            obj = _filler.Fill(vm, obj);
            _repo.Edit(obj);

            try
            {
                _repo.Commit();
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            if (postActionCallback != null)
            {
                postActionCallback.Invoke(obj);
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}