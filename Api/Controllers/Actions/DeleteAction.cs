using System.Net;
using System.Net.Http;
using NHibernateBootstrap;

namespace PoupeMe.Api.Controllers.Actions
{
    public class DeleteAction<TEntity>
        where TEntity : class, IHaveId 
    {
        private readonly IRepository<TEntity> _repo;

        public DeleteAction(IRepository<TEntity> repo)
        {
            _repo = repo;
        }

        public HttpResponseMessage Do(int id)
        {
            var obj = _repo.Find(id);
            if (obj == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            _repo.Remove(obj);

            try
            {
                _repo.Commit();
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}