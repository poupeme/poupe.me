using System.Collections.Generic;
using System.Linq;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.ViewModels.Budget;

namespace PoupeMe.Api.Controllers.Actions
{
    public class BudgetMonthAction
    {
        public virtual List<BudgetItemVm> Do(int monthId, IEnumerable<BudgetItem> allBudgetItems, IEnumerable<Transaction> allTransactions)
        {
            var monthNumber = monthId + 1;
            var budgetItems = allBudgetItems.Where(b => b.Period.Month == monthNumber).ToList();
            var transactionsGroupedByDealer = GetTransactionsGroupedByDealer(allTransactions, monthNumber);

            var budget = new List<BudgetItemVm>();

            foreach (var transactionGroup in transactionsGroupedByDealer)
            {
                var localTransactionGroup = transactionGroup;
                var budgetItem = budgetItems.FirstOrDefault(b => b.Category.Id == localTransactionGroup.CategoryId);

                if (budgetItem != null)
                {
                    transactionGroup.PlannedValue = budgetItem.Value;
                    transactionGroup.Memo = budgetItem.Memo;
                    budgetItems.Remove(budgetItem);
                }

                budget.Add(transactionGroup);
            }

            if (budgetItems.All(b => b.Category != null))
            {
                budgetItems.Add(new BudgetItem());
            }
            budget.AddRange(budgetItems.Select(budgetItem => new BudgetItemVm
            {
                CategoryId = (budgetItem.Category != null) ? budgetItem.Category.Id : 0,
                RealValue = 0,
                PlannedValue = budgetItem.Value,
                Memo = budgetItem.Memo
            }));
            return budget;
        }

        private static IEnumerable<BudgetItemVm> GetTransactionsGroupedByDealer(IEnumerable<Transaction> allTransactions, int monthNumber)
        {
            var transactions = allTransactions.Where(t => t.Date.Month == monthNumber).ToList();

            var expenses = transactions
                .Where(t => t.SourceDealer.Type == DealerType.Account && t.DestinyDealer.Type != DealerType.Account)
                .GroupBy(t => t.DestinyDealer);

            var incomings = transactions
                .Where(t => t.DestinyDealer.Type == DealerType.Account && t.SourceDealer.Type != DealerType.Account)
                .GroupBy(t => t.SourceDealer);

            return expenses
                .ToList()
                .Union(incomings)
                .Select(g => new BudgetItemVm
                {
                    CategoryId = g.Key.Id,
                    RealValue = g.Sum(t => t.Value),
                    Memo = ""
                });
        }
    }
}