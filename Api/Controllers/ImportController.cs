using System;
using System.Collections;
using System.Net.Http;
using System.Threading.Tasks;
using JetBrains.Annotations;
using PoupeMe.Api.Components;
using PoupeMe.Api.Controllers.Actions;
using PoupeMe.Api.Models.Repositories;
using PoupeMe.Api.Models.ViewModels.Transaction;

namespace PoupeMe.Api.Controllers
{
    public class ImportController: ApiControllerBase
    {
        private readonly TransactionImportAction _importAction;
        private readonly TransactionFromFileAction _fromFileAction;

        public ImportController()
        {
            var classifierRepo = new Lazy<ClassifierRepo>(() => new ClassifierRepo(Db.UserRepository, User.Identity.Name));
            var hashCreator = new HashCreator();

            _importAction = new TransactionImportAction(Db.TransactionRepository, Db.DealerRepository, classifierRepo);
            _fromFileAction = new TransactionFromFileAction(Db.TransactionRepository, hashCreator, classifierRepo);
        }

        [UsedImplicitly]
        public HttpResponseMessage Put(TransactionImportListVm vm)
        {
            return _importAction.Do(vm);
        }

        [UsedImplicitly]
        public Task<IEnumerable> Post()
        {
            return _fromFileAction.Do(this);
        }
    }
}