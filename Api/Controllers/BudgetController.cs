﻿using System.Net.Http;
using JetBrains.Annotations;
using PoupeMe.Api.Controllers.Actions;
using PoupeMe.Api.Models.ViewModels.Budget;

namespace PoupeMe.Api.Controllers
{
    public class BudgetController : ApiControllerBase
    {
        private readonly BudgetPostAction _postAction;
        private readonly BudgetGetAction _getAction;

        public BudgetController()
        {
            var budgetMonthAction = new BudgetMonthAction();
            _postAction = new BudgetPostAction(Db.BudgetItemRepository, Db.DealerRepository);
            _getAction = new BudgetGetAction(Db.BudgetItemRepository, Db.TransactionRepository, budgetMonthAction, Db.Operations);
        }

        [UsedImplicitly]
        public BudgetVm Get(int year)
        {
            return _getAction.Do(year);
        }

        [UsedImplicitly]
        public HttpResponseMessage Put(BudgetVm vm)
        {
            return _postAction.Do(vm);
        }
    }
}
