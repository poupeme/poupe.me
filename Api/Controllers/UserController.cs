﻿using System.Linq;
using System.Web.Http;
using JetBrains.Annotations;
using PoupeMe.Api.Components;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.Fillers;
using PoupeMe.Api.Models.ViewModels.User;

namespace PoupeMe.Api.Controllers
{
    public class UserController : ApiControllerBase
    {
        private readonly UserVmFiller _filler;

        public UserController()
        {
            _filler = new UserVmFiller();
        }

        [HttpGet]
        [UsedImplicitly]
        public UserVm Get()
        {
            return _filler.Fill(Db.User);
        }

        /*[HttpPost]
        [UsedImplicitly]
        public UserVm Signup(UserSignupVm vm)
        {
            if (Db.UserRepository.AsQueryable().Any(u => u.Email == vm.Email))
            {
                //todo:ModelState.AddModelError("Email", Resource.CantRegisterBecauseAUserWithSameEmailAlreadyExists);
                //return View(vm);
            }
            if (Db.UserRepository.AsQueryable().Any(u => u.UserName == vm.UserName))
            {
                //todo:ModelState.AddModelError("Email", Resource.CantRegisterBecauseAUserWithSameNameAlreadyExists);
                //return View(vm);
               }
            var user = new User
            {
                Email = vm.Email.ToLowerInvariant(),
                FullName = vm.FullName,
                Password = vm.Password.ToPasswordHash(),
                UserName = vm.UserName.ToLowerInvariant(),
                //todo: CreatedAt = DateTime.Now
            };
            Db.UserRepository.Add(user);
            Db.Commit();

            return _filler.Fill(user);
        }*/
    }
}
