﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using JetBrains.Annotations;
using PoupeMe.Api.Controllers.Actions;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.Fillers;
using PoupeMe.Api.Models.ViewModels.Category;

namespace PoupeMe.Api.Controllers
{
    public class CategoryController : ApiControllerBase
    {
        private readonly GetAction<CategoryVm, Dealer> _getAction;
        private readonly PostAction<CategoryVm, Dealer> _postAction;
        private readonly PutAction<CategoryVm, Dealer> _putAction;
        private readonly DeleteAction<Dealer> _deleteAction;
        private readonly ListAction<CategoryVm, Dealer> _listAction;

        public CategoryController()
        {
            var accountGetFiller = new CategoryVmFiller();
            var accountFiller = new CategoryFiller();
            _getAction = new GetAction<CategoryVm, Dealer>(Db.AccountRepository, accountGetFiller);
            _postAction = new PostAction<CategoryVm, Dealer>(Db.AccountRepository, accountFiller);
            _putAction = new PutAction<CategoryVm, Dealer>(Db.AccountRepository, accountFiller);
            _deleteAction = new DeleteAction<Dealer>(Db.AccountRepository);
            _listAction = new ListAction<CategoryVm,Dealer>(Db.AccountRepository, accountGetFiller);
        }

        [UsedImplicitly]
        public IEnumerable<CategoryVm> Get()
        {
            return _listAction.Do(d => d.Name);
        }

        [UsedImplicitly]
        public CategoryVm Get(int id)
        {
            return _getAction.Do(id);
        }

        [UsedImplicitly]
        public HttpResponseMessage Post(CategoryVm vm)
        {
            return _postAction.Do(this, vm);
        }

        [HttpPut]
        [UsedImplicitly]
        public HttpResponseMessage Put(int id, CategoryVm vm)
        {
            return _putAction.Do(id, vm);
        }

        [HttpDelete]
        [UsedImplicitly]
        public HttpResponseMessage Delete(int id)
        {
            return _deleteAction.Do(id);
        }
    }
}