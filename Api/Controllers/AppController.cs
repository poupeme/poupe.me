﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using JetBrains.Annotations;
using PoupeMe.Api.Components;
using PoupeMe.Api.Models;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.ViewModels.App;

namespace PoupeMe.Api.Controllers
{
    public class AppController : ApiControllerBase
    {
        [HttpGet]
        [UsedImplicitly]
        public AppDataVm Data()
        {
            var accounts = Db.AccountRepository.AsQueryable().OrderBy(d => d.Name).ThenBy(d => d.Type).ToList();
            var category = Db.CategoryRepository.AsQueryable().OrderBy(d => d.Name).ThenBy(d => d.Type).ToList();

            var dealers = new List<Dealer>();
            dealers.AddRange(accounts);
            dealers.AddRange(category);

            return new AppDataVm
            {
                Accounts = GetNamedDictionary(accounts),
                Categories = GetTypedDictionary(category),
                Dealers = GetNamedDictionary(dealers),
                CategoryTypes = EnumListGenerator.Generate<DealerType>().Where(d => d.Id != 0),
                IncomingDealers = GetTypedDictionary(dealers, d => d.Type == DealerType.Account || d.Type == DealerType.Incoming),
                ExpenseDealers = GetTypedDictionary(dealers, d => d.Type == DealerType.Account || d.Type == DealerType.Expense),
                CurrentMonth = DateTime.Today.Month - 1,
                CurrentYear = DateTime.Today.Year
            };
        }

        #region Private Methods

        private IEnumerable<T> GetDictionary<T>(IEnumerable<Dealer> items, Func<Dealer, T> select, Func<Dealer, bool> filter = null)
        {
            return (filter == null ? items : items.Where(filter)).Select(@select);
        }

        private IEnumerable<NamedEntity> GetNamedDictionary(IEnumerable<Dealer> items, Func<Dealer, bool> filter = null)
        {
            return GetDictionary(items, d => new NamedEntity { Id = d.Id, Name = d.Name }, filter);
        }

        private IEnumerable<TypedEntity> GetTypedDictionary(IEnumerable<Dealer> items, Func<Dealer, bool> filter = null)
        {
            return GetDictionary(items, d => new TypedEntity { Id = d.Id, Name = d.Name, Type = (int)d.Type }, filter);
        }
        #endregion
    }
}