﻿using System.Web.Http;
using System.Web.Http.Controllers;
using JWT;
using Newtonsoft.Json;
using PoupeMe.Api.Components;
using PoupeMe.Api.Models.Interfaces;
using PoupeMe.Api.Models.Repositories;
using PoupeMe.Api.Models.ViewModels.User;

namespace PoupeMe.Api.Controllers
{
    public class ApiControllerBase : ApiController
    {
        private IUnitOfWork _db;

        protected IUnitOfWork Db
        {
            get
            {
                return _db ?? (_db = new UnitOfWork());
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void Initialize(HttpControllerContext context)
        {
            if (context.Request.Headers.Authorization == null) return;
            UserVm user;
            try
            {
                var userStr = JsonWebToken.Decode(context.Request.Headers.Authorization.Parameter,
                    Constants.TokenPassword);
                user = JsonConvert.DeserializeObject<UserVm>(userStr);
                if (user == null)
                {
                    return;
                }
            }
            catch
            {
                return;
            }
            Db.Login(user.Id);
        }
    }
}