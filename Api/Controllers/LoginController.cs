using System.Net;
using System.Web.Http;
using JetBrains.Annotations;
using JWT;
using PoupeMe.Api.Components;
using PoupeMe.Api.Models.Fillers;
using PoupeMe.Api.Models.ViewModels.Login;

namespace PoupeMe.Api.Controllers
{
    public class LoginController : ApiControllerBase
    {
        [HttpPost]
        [UsedImplicitly]
        public LoginVm Post(LoginVm login)
        {
            var userVmFiller = new UserVmFiller();

            if (Db.Login(login.Email, login.Password))
            {
                return new LoginVm
                {
                    AccessToken =
                        JsonWebToken.Encode(userVmFiller.Fill(Db.User), Constants.TokenPassword, JwtHashAlgorithm.HS256)
                };
            }
            throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }
    }
}