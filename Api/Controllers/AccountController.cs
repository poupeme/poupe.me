﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using JetBrains.Annotations;
using PoupeMe.Api.Controllers.Actions;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.Fillers;
using PoupeMe.Api.Models.ViewModels.Account;

namespace PoupeMe.Api.Controllers
{
    public class AccountController : ApiControllerBase
    {
        private readonly GetAction<AccountVm, Dealer> _getAction;
        private readonly PostAction<AccountVm, Dealer> _postAction;
        private readonly PutAction<AccountVm, Dealer> _putAction;
        private readonly DeleteAction<Dealer> _deleteAction;
        private readonly ListAction<AccountVm, Dealer> _listAction;

        public AccountController()
        {
            var accountGetFiller = new AccountVmFiller(Db.Operations);
            var accountFiller = new AccountFiller();
            _getAction = new GetAction<AccountVm, Dealer>(Db.AccountRepository, accountGetFiller);
            _postAction = new PostAction<AccountVm, Dealer>(Db.AccountRepository, accountFiller);
            _putAction = new PutAction<AccountVm, Dealer>(Db.AccountRepository, accountFiller);
            _deleteAction = new DeleteAction<Dealer>(Db.AccountRepository);
            _listAction = new ListAction<AccountVm, Dealer>(Db.AccountRepository, accountGetFiller);
        }

        [UsedImplicitly]
        public IEnumerable<AccountVm> Get()
        {
            return _listAction.Do(d => d.Name);
        }

        [UsedImplicitly]
        public AccountVm Get(int id)
        {
            return _getAction.Do(id);
        }

        [UsedImplicitly]
        public HttpResponseMessage Post(AccountVm vm)
        {
            return _postAction.Do(this, vm);
        }

        [HttpPut]
        [UsedImplicitly]
        public HttpResponseMessage Put(int id, AccountVm vm)
        {
            return _putAction.Do(id, vm);
        }

        [HttpDelete]
        [UsedImplicitly]
        public HttpResponseMessage Delete(int id)
        {
            return _deleteAction.Do(id);
        }
    }
}