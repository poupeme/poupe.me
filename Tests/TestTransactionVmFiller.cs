using NUnit.Framework;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestTransactionVmFiller
    {
        [Test]
        public void TransactionVmFiller_Fill_Success()
        {
            var filler = new TransactionVmFiller();
            var result = filler.Fill(TestToolkit.DemoTransaction);
            AssertEx.PropertyValuesAreEquals(TestToolkit.DemoTransactionVm, result);
        }

        [Test]
        public void TransactionVmFiller_Fill_SuccessWithNullDealers()
        {
            var filler = new TransactionVmFiller();
            var result = filler.Fill(new Transaction());
            Assert.AreEqual(null, result.DestinyDealerId);
            Assert.AreEqual(null, result.SourceDealerId);
        }
    }
}