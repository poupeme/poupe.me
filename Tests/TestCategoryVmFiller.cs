using NUnit.Framework;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestCategoryVmFiller
    {
        [Test]
        public void CategoryVmFiller_Fill_Success()
        {
            var filler = new CategoryVmFiller();
            var result = filler.Fill(TestToolkit.DemoCategory);
            AssertEx.PropertyValuesAreEquals(TestToolkit.DemoCategoryVm, result);
        }
    }
}