﻿using System.Collections.Generic;
using System.Linq;
using NHibernateBootstrap;

namespace PoupeMe.Api.Tests
{
    public class MemoryRepository<TEntity> : IRepository<TEntity> where TEntity: IHaveId
    {
        private readonly List<TEntity> _items;

        public MemoryRepository(List<TEntity> items)
        {
            _items = items;
        }

        public TEntity Find(int id)
        {
            return _items.Find(e => e.Id == id);
        }

        public IQueryable<TEntity> AsQueryable()
        {
            return _items.AsQueryable();
        }

        public void Add(TEntity obj)
        {
            _items.Add(obj);
        }

        public void Edit(TEntity obj)
        {
            //do nothing
        }

        public void Remove(TEntity obj)
        {
            _items.Remove(obj);
        }

        public void Remove(int id)
        {
            Remove(Find(id));
        }

        public void Commit()
        {
            //do nothing
        }
    }
}