using NUnit.Framework;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestCategoryFiller
    {
        [Test]
        public void CategoryFiller_Fill_Success()
        {
            var filler = new CategoryFiller();
            var result = filler.Fill(TestToolkit.DemoCategoryVm);
            AssertEx.PropertyValuesAreEquals(TestToolkit.DemoCategory, result);
        }
    }
}