using System.Net;
using System.Net.Http;
using Moq;
using NHibernateBootstrap;
using NUnit.Framework;
using PoupeMe.Api.Controllers.Actions;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestPostAction
    {
        private Mock<IRepository<FooEntity>> _repoMock;
        private FooEntityVm _fooEntityVm;
        private FooEntity _fooEntity;
        private Mock<IEntityFiller<FooEntityVm, FooEntity>> _filler;

        [Test]
        public void PostAction_Do_Success()
        {
            //setup
            Setup();
            _repoMock.Setup(m => m.Commit()).Callback(() => _fooEntity.Id = 1);

            //excercise
            var action = new PostAction<FooEntityVm, FooEntity>(_repoMock.Object, _filler.Object);
            var responseMessage = action.Do(null, _fooEntityVm);

            //verify
            Verify(responseMessage, "1");
        }

        [Test]
        public void PostAction_Do_WithCallback()
        {
            Setup();

            //excercise
            var action = new PostAction<FooEntityVm, FooEntity>(_repoMock.Object, _filler.Object);
            var responseMessage = action.Do(null, _fooEntityVm, e => e.Id = 123);

            //verify
            Verify(responseMessage, "123");
        }

        #region Private Methods

        private void Setup()
        {
            _fooEntityVm = new FooEntityVm();
            _fooEntity = new FooEntity();
            _repoMock = new Mock<IRepository<FooEntity>>();
            _filler = new Mock<IEntityFiller<FooEntityVm, FooEntity>>();
            _filler.Setup(m => m.Fill(_fooEntityVm, null)).Returns(_fooEntity);
        }

        private void Verify(HttpResponseMessage responseMessage, string expectedResult)
        {
            Assert.AreEqual("Created", responseMessage.ReasonPhrase);
            Assert.AreEqual(HttpStatusCode.Created, responseMessage.StatusCode);
            Assert.AreEqual(expectedResult, responseMessage.Content.ReadAsStringAsync().Result);

            _repoMock.Verify(m => m.Add(_fooEntity));
            _repoMock.Verify(m => m.Commit());
        }

        #endregion
    }
}