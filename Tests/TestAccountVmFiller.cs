using Moq;
using NUnit.Framework;
using PoupeMe.Api.Models.Fillers;
using PoupeMe.Api.Models.Interfaces;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestAccountVmFiller
    {
        [Test]
        public void AccountVmFiller_Fill_Success()
        {
            var operationsMock = new Mock<IExtraDbOperations>();
            operationsMock.Setup(m => m.GetAccountBalance(TestToolkit.DemoAccount)).Returns(1000);
            var filler = new AccountVmFiller(operationsMock.Object);
            var result = filler.Fill(TestToolkit.DemoAccount);
            AssertEx.PropertyValuesAreEquals(TestToolkit.DemoAccountVm, result);
        }
    }
}