﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Dongle.System.IO;
using NUnit.Framework;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class ComplianceRulesTest
    {
        /*[Test]
        public void AllAngularResourcesHaveResource()
        {
            var files = ApiDirectory.GetFiles("*.*html", SearchOption.AllDirectories);

            foreach (var fileInfo in files)
            {
                var content = fileInfo.GetContent();
                var lines = content.Split('\n');

                for (var i = 0; i < lines.Length; i++)
                {
                    var line = lines[i];

                    var matches = Regex.Matches(line, @"{{\s*appData.Strings.(\w+)\s*}}");

                    foreach (Match match in matches)
                    {
                        var value = match.Groups[1].Captures[match.Groups[1].Captures.Count - 1].Value;
                        var str = Resource.ResourceManager.GetString(value);

                        if (string.IsNullOrEmpty(str))
                        {
                            Assert.Fail("{0} encontrado Angular Resource sem resource: \"{1}\". linha {2}",
                                fileInfo.FullName, value, i);
                        }
                    }
                }
            }
        }*/

        /*[Test]
        public void AllResourcesAreBeingUsed()
        {
            var ignoreList = new List<string> {"Month0", "Month1", "Month10", "Month11", "Month2", "Month3", "Month4", "Month5", "Month6", "Month7", "Month8", "Month9"};
            var resFile = ApiDirectory.GetFiles("Resource.resx", SearchOption.AllDirectories).FirstOrDefault();

            var htmlFiles = ApiDirectory.GetFiles("*.*html", SearchOption.AllDirectories);
            var csFiles = ApiDirectory.GetFiles("*.cs", SearchOption.AllDirectories);
            var jsFiles = ApiDirectory.GetFiles("*.js", SearchOption.AllDirectories);

            var content = htmlFiles.Aggregate("", (current, fileInfo) => current + fileInfo.GetContent());
            content += csFiles.Aggregate("", (current, fileInfo) => current + fileInfo.GetContent());
            content += jsFiles.Aggregate("", (current, fileInfo) => current + fileInfo.GetContent());

            var keys = GetResourceKeys(resFile);
            var errors = keys.Where(key => !content.Contains("Resource." + key) 
                //&& !content.Contains("appData.Strings." + key)
                //&& !content.Contains("appDataResource.Strings." + key)
                && !ignoreList.Contains(key)).ToList();

            if (errors.Count > 0)
            {
                Assert.Fail("Encontrado resources não usados: {0}", errors.Aggregate("", (current, s) => current + ", " + s));
            }
        }*/

        /*[Test]
        public void AllCssClassesAreBeingUsed()
        {
            var whitelist = new List<string>
            {
                "bar",// angular-loading-bar/loading-bar.js
                "user-logo",// usado no Gravatar extension method
                "value-title-warning",
                "value-title-success",
                "value-title-danger",
                "value-title-extra-danger",
                "value-title-extra-success",
                "progress-bar-success",
                "progress-bar-warning",
                "progress-bar-danger",
                "progress-bar-extra-success",
                "progress-bar-extra-danger",
            };

            var htmlFiles = ApiDirectory.GetFiles("*.*html", SearchOption.AllDirectories);

            var html = htmlFiles.Aggregate("", (current, htmlFile) => current + htmlFile.GetContent());

            var matches = Regex.Matches(html, @"class\s*=\s*[""]([^""]+)[""]");
            var matches2 = Regex.Matches(html, @"class\s*=\s*[']([^']+)[']");
            var m = matches.Cast<Match>().Union(matches2.Cast<Match>());

            var htmlClasses = m.SelectMany(mm => Regex.Matches(mm.Groups[1].Value, "[_a-zA-Z]+[_a-zA-Z0-9-]*")
                .Cast<Match>()
                .Select(m2 => m2.Value))
                .ToList();
            
            var stylesDir = ApiDirectory.GetDirectories("app/styles").FirstOrDefault();
            if (stylesDir != null)
            {
                var cssFiles = stylesDir.GetFiles("*.css", SearchOption.AllDirectories);
                foreach (var cssFile in cssFiles)
                {
                    var content = cssFile.GetContent();
                    var classes = Regex.Matches(content, @"\.([_a-zA-Z]+[_a-zA-Z0-9-]*)\s*\{");

                    foreach (Match match in classes)
                    {
                        var className = match.Groups[1].Value;

                        if (!htmlClasses.Contains(className) && !whitelist.Contains(className))
                        {
                            Assert.Fail(@"classe "".{0}"" no arquivo {1} não está sendo usada em nenhum html", className,
                                cssFile.FullName);
                        }
                    }
                }
            }
            else
            {
                Assert.Fail("Não encontrou o diretório de estilos");
            }
        }*/

        [Test]
        public void AssertAllResourcesExistsInAllLanguages()
        {
            var files = ApiDirectory.GetFiles("Resource*.resx", SearchOption.AllDirectories);
            var missingKeys = string.Empty;
            for (var i = 0; i < files.Length; i++)
            {
                var fileInfo = files[i];
                var keys = GetResourceKeys(fileInfo);
                const string errorMessage = "Arquivo {0} contém a chave {1}, mas arquivo {2} não contém.\"\n\"";

                for (var j = 0; j < files.Length; j++)
                {
                    if (j == i) continue;
                    var jKeys = GetResourceKeys(files[j]);

                    var j1 = j;
                    missingKeys = jKeys
                        .Where(jKey => !keys.Contains(jKey))
                        .Aggregate(missingKeys,
                            (current, jKey) =>
                                current + string.Format(errorMessage, files[j1].Name, jKey, fileInfo.Name));
                }
            }
            Assert.IsTrue(string.IsNullOrEmpty(missingKeys), missingKeys);
        }

        [Test]
        public void AssertAllNamespacesAreCorrect()
        {
            var files = ApiDirectory.GetFiles("*.cs", SearchOption.AllDirectories);

            foreach (var fileInfo in files)
            {
                if (fileInfo.FullName.Contains("\\obj\\") || fileInfo.Name == "AssemblyInfo.cs" || fileInfo.Name.Contains("Designer.cs") || fileInfo.FullName.Contains("App_Start"))
                {
                    continue;
                }
                var content = fileInfo.GetContent();
                var matches = Regex.Matches(content, @"namespace\s+([\w\.]+)");
                Assert.IsTrue(matches.Count == 1, "Não pode haver mais de um namespace no arquivo, e tem que haver pelo menos 1: {0}", fileInfo.FullName);

                if (fileInfo.Directory != null)
                {
                    var @namespace = "PoupeMe.Api" +
                                     fileInfo.Directory.FullName.Substring(ApiDirectory.FullName.Length)
                                         .Replace("\\", ".");
                    var namespaceFound = matches[0].Groups[1].Value;

                    Assert.AreEqual(@namespace, namespaceFound, "Namespace errado. Arquivo: {0}", fileInfo.FullName);
                }
                else
                {
                    Assert.Fail("Diretorio do arquivo {0} não foi encontrado", fileInfo.FullName);
                }
            }
        }

        [Test]
        public void AssertOnlyOneClassPerFile()
        {
            var files = ApiDirectory.GetFiles("*.cs", SearchOption.AllDirectories);

            foreach (var fileInfo in files)
            {
                if (fileInfo.FullName.Contains("\\obj\\") || fileInfo.Name == "AssemblyInfo.cs" || fileInfo.Name.Contains("Designer.cs") || fileInfo.Name.Contains("Resource.cs"))
                {
                    continue;
                }

                var content = fileInfo.GetContent();
                var matches = Regex.Matches(content, @"(interface|class|enum)\s+\w[\w\<\,\s\>]+\s*(\:\s*[\w\.\,\<\>\:\(\)\s]+\s+)*{");
                Assert.IsTrue(matches.Count == 1, "Não pode haver mais de uma classe namespace no arquivo, e tem que haver pelo menos 1: {0}", fileInfo.FullName);
            }
        }

        /*[Test]
        public void AssertAllClassesAreCorrectlyNamed()
        {
            var files = ApiDirectory.GetFiles("*.cs", SearchOption.AllDirectories);

            foreach (var fileInfo in files)
            {
                if (fileInfo.Name == "AssemblyInfo.cs" || fileInfo.Name == "Global.asax.cs" || fileInfo.Name.Contains("Designer.cs"))
                {
                    continue;
                }
                var content = fileInfo.GetContent();
                var matches = Regex.Matches(content, @"(?:interface|class|enum)\s+(\w+)");
                var classFound = matches[0].Groups[1].Value;

                var @class = fileInfo.Name.Substring(0, fileInfo.Name.Length - fileInfo.Extension.Length);

                Assert.AreEqual(@class, classFound, "Classe com nome diferente do arquivo: {0}", fileInfo.FullName);
            }
        }*/

        [Test]
        public void AssertViewModelsAreCorrectlyNamed()
        {
            var siteDir = ApiDirectory.GetDirectories("Models/ViewModels").FirstOrDefault();
            if (siteDir != null)
            {
                var files = siteDir.GetFiles("*.cs", SearchOption.AllDirectories);

                foreach (var fileInfo in files)
                {
                    const string suffix = "Vm.cs";
                    Assert.IsTrue(fileInfo.Name.EndsWith(suffix), "Classe ViewModel precisa terminar com {0}: {1}",
                        suffix, fileInfo.FullName);
                    if (fileInfo.Directory != null)
                    {
                        var prefix = fileInfo.Directory.Name;
                        Assert.IsTrue(fileInfo.Name.StartsWith(prefix), "Classe ViewModel precisa começar com {0}: {1}",
                            prefix, fileInfo.FullName);
                    }
                    else
                    {
                        Assert.Fail("Diretório não encontrado.");
                    }
                }
            }
            else
            {
                Assert.Fail("Diretório ViewModels não encontrado.");
            }
        }

        [Test]
        public void AssertControllersAreCorrectlyNamed()
        {
            var files = ApiDirectory.GetFiles("*.cs", SearchOption.AllDirectories);

            foreach (var fileInfo in files)
            {
                if (fileInfo.Directory != null && fileInfo.Directory.Name == "Controllers")
                {
                    Assert.IsTrue(fileInfo.Name.EndsWith("Controller.cs") || fileInfo.Name.EndsWith("ControllerBase.cs"), "Controller precisa terminar com Controller ou ControllerBase: {0}", fileInfo.FullName);
                }
            }
        }

        #region Private Methods

        private static List<string> GetResourceKeys(FileInfo fileInfo)
        {
            var quoteDocument = XDocument.Parse(fileInfo.GetContent());
            if (quoteDocument.Root != null)
            {
                return
                    quoteDocument.Root.Elements()
                        .Where(e => e.Name.LocalName == "data")
                        .Select(e => e.FirstAttribute.Value)
                        .ToList();
            }
            Assert.Fail("Não pode carregar o resource {0}", fileInfo.FullName);
            return null;
        }

        private static DirectoryInfo ApiDirectory
        {
            get { return RootDirectory.GetDirectories("Api").FirstOrDefault(); }
        }

        private static DirectoryInfo RootDirectory
        {
            get
            {
                var baseDir = new DirectoryInfo(ApplicationPaths.RootDirectory);
                while (true)
                {
                    if (baseDir == null) break;
                    var foundBaseDir = baseDir.GetFiles("*.sln").Any();
                    if (foundBaseDir) break;
                    baseDir = baseDir.Parent;
                }
                return baseDir;
            }
        }

        #endregion
    }
}
