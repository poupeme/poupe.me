﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NHibernateBootstrap;
using NUnit.Framework;
using PoupeMe.Api.Controllers;
using PoupeMe.Api.Controllers.Actions;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.Interfaces;
using PoupeMe.Api.Models.ViewModels.Budget;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    internal class TestBudgetGetAction
    {
        [Test]
        public void BudgetGetAction_Do_Success()
        {
            //setup
            const int year = 2010;
            var budgetItems = new List<BudgetItem>();
            var transactions = new List<Transaction>();
            var bis = new List<List<BudgetItemVm>>();

            var monthActionMock = new Mock<BudgetMonthAction>();
            var repoMock = new Mock<IRepository<BudgetItem>>();
            var transactionRepoMock = new Mock<IRepository<Transaction>>();
            var operationsMock = new Mock<IExtraDbOperations>();

            repoMock.Setup(r => r.AsQueryable()).Returns(budgetItems.AsQueryable);
            transactionRepoMock.Setup(t => t.AsQueryable()).Returns(transactions.AsQueryable);
            operationsMock.Setup(m => m.GetYearOpeningBalance(year)).Returns(123.45M);

            for (var monthNumber = 0; monthNumber < 12; monthNumber++)
            {
                var number = monthNumber;
                bis.Add(new List<BudgetItemVm>());
                monthActionMock.Setup(m => m.Do(number, budgetItems, transactions)).Returns(bis[monthNumber]);
            }

            //excercise
            var action = new BudgetGetAction(repoMock.Object, transactionRepoMock.Object, monthActionMock.Object, operationsMock.Object);
            var result = action.Do(year);

            //verify
            Assert.AreEqual(123.45M, result.LastBalance);
            Assert.AreEqual(DateTime.Today, result.Today); 
            Assert.AreEqual(year, result.Year);
            for (var month = 0; month < 12; month++)
            {
                Assert.AreEqual(bis[month], result.Budgets[month]);
            }
        }
    }
}
