using System;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.ViewModels.Account;
using PoupeMe.Api.Models.ViewModels.Category;
using PoupeMe.Api.Models.ViewModels.Transaction;
using PoupeMe.Api.Models.ViewModels.User;

namespace PoupeMe.Api.Tests
{
    public static class TestToolkit
    {
        public static readonly TransactionVm DemoTransactionVm = new TransactionVm
        {
            Date = new DateTime(2010, 1, 1),
            DestinyDealerId = 2,
            Hash = "ABCDEF",
            Id = 1,
            Memo = "Demo Memo",
            SourceDealerId = 1,
            Value = 12.34m
        };

        public static readonly TransactionVm DemoUnhashedTransactionVm = new TransactionVm
        {
            Date = new DateTime(2010, 1, 1),
            DestinyDealerId = 2,
            Id = 1,
            Memo = "Demo Memo",
            SourceDealerId = 1,
            Value = 12.34m
        };

        public static readonly UserVm DemoUserVm = new UserVm
        {
            Id = 1,
            //todo: CreatedAt = new DateTime(2010, 1, 1),
            Email = "user1@poupe.me",
            FullName = "User 1",
            UserName = "user1"
        };

        public static readonly User DemoUser = new User
        {
            Id = 1,
            //todo: CreatedAt = new DateTime(2010, 1, 1),
            Email = "user1@poupe.me",
            FullName = "User 1",
            UserName = "user1",
            Password = "123456"
        };

        public static readonly Dealer DemoCategory = new Dealer
        {
            Id = 1,
            Name = "Alimentação",
            Type = DealerType.Expense
        };

        public static readonly Dealer DemoAccount = new Dealer
        {
            Id = 2,
            Name = "Carteira",
            OpeningBalance = 100,
            Type = DealerType.Account,
        };

        public static readonly Transaction DemoTransaction = new Transaction
        {
            Date = new DateTime(2010, 1, 1),
            DestinyDealer = DemoAccount,
            Hash = "ABCDEF",
            Id = 1,
            Memo = "Demo Memo",
            SourceDealer = DemoCategory,
            Value = 12.34m
        };

        public static readonly CategoryVm DemoCategoryVm = new CategoryVm
        {
            Id = 1,
            Name = "Alimentação",
            Type = DealerType.Expense
        };

        public static readonly AccountVm DemoAccountVm = new AccountVm
        {
            Id = 2,
            Name = "Carteira",
            OpeningBalance = 100,
            Balance = 1000
        };

    }
}