using NUnit.Framework;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestAccountFiller
    {
        [Test]
        public void AccountFiller_Fill_Success()
        {
            var filler = new AccountFiller();
            var result = filler.Fill(TestToolkit.DemoAccountVm);
            AssertEx.PropertyValuesAreEquals(TestToolkit.DemoAccount, result);
        }
    }
}