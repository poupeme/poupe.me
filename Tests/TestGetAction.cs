using System.Net;
using System.Web.Http;
using Moq;
using NHibernateBootstrap;
using NUnit.Framework;
using PoupeMe.Api.Controllers.Actions;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestGetAction
    {
        [Test]
        public void GetAction_Do_Success()
        {
            //setup
            var fooEntityVm = new FooEntityVm();
            var fooEntity = new FooEntity();

            var repoMock = new Mock<IRepository<FooEntity>>();
            repoMock.Setup(m => m.Find(1)).Returns(fooEntity);

            var filler = new Mock<IEntityFiller<FooEntity, FooEntityVm>>();
            filler.Setup(m => m.Fill(fooEntity, null)).Returns(fooEntityVm);

            //excercise
            var action = new GetAction<FooEntityVm, FooEntity>(repoMock.Object, filler.Object);
            var vm = action.Do(1);

            //verify
            Assert.AreEqual(fooEntityVm, vm);
        }

        [Test]
        public void GetAction_Do_NotFound()
        {
            var repoMock = new Mock<IRepository<FooEntity>>();
            repoMock.Setup(m => m.Find(1)).Returns((FooEntity)null);
            var filler = new Mock<IEntityFiller<FooEntity, FooEntityVm>>();
            var action = new GetAction<FooEntityVm, FooEntity>(repoMock.Object, filler.Object);
            var exception = Assert.Throws<HttpResponseException>(() => action.Do(1));
            Assert.AreEqual(HttpStatusCode.NotFound, exception.Response.StatusCode);
        }
    }
}