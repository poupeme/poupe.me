using NUnit.Framework;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestUserVmFiller
    {
        [Test]
        public void UserVmFiller_Fill_Success()
        {
            var filler = new UserVmFiller();
            var result = filler.Fill(TestToolkit.DemoUser);
            AssertEx.PropertyValuesAreEquals(TestToolkit.DemoUserVm, result);
        }
    }
}