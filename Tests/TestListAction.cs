using System.Collections.Generic;
using System.Linq;
using Moq;
using NHibernateBootstrap;
using NUnit.Framework;
using PoupeMe.Api.Controllers.Actions;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestListAction
    {
        private Mock<IRepository<FooEntity>> _repoMock;
        private Mock<IEntityFiller<FooEntity, FooEntityVm>> _filler;

        [Test]
        public void ListAction_Do_SuccessWhereAndOrder()
        {
            var action = Setup();
            var result = action.Do(e => e.Id > 1, e => e.Id);
            Verify(result);
        }
        
        [Test]
        public void ListAction_Do_SuccessOrder()
        {
            var action = Setup();
            var result = action.Do(e => e.Id);
            Verify(result);
        }

        private static void Verify(List<FooEntityVm> result)
        {
            Assert.AreEqual(2, result.Count());
            Assert.AreEqual(2, result[0].Id);
            Assert.AreEqual(3, result[1].Id);
        }

        private ListAction<FooEntityVm, FooEntity> Setup()
        {
            var obj3 = new FooEntity {Id = 3};
            var obj2 = new FooEntity {Id = 2};
            var items = new List<FooEntity> {obj3, obj2};

            _filler = new Mock<IEntityFiller<FooEntity, FooEntityVm>>();
            _filler.Setup(m => m.Fill(obj3, null)).Returns(new FooEntityVm {Id = 3});
            _filler.Setup(m => m.Fill(obj2, null)).Returns(new FooEntityVm {Id = 2});

            _repoMock = new Mock<IRepository<FooEntity>>();
            _repoMock.Setup(m => m.AsQueryable()).Returns(items.AsQueryable);

            //excercise
            var action = new ListAction<FooEntityVm, FooEntity>(_repoMock.Object, _filler.Object);
            return action;
        }
    }
}