using System;
using NUnit.Framework;
using PoupeMe.Api.Components;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestHashCreator
    {
        [Test]
        public void HashCreator_Create_Success()
        {
            //setup
            var hashCreator = new HashCreator();

            //excercise
            var result = hashCreator.Create(new DateTime(2010, 1, 1), "Memo", 12.34m);

            //verify
            Assert.IsTrue(result == "Q8H1N6" || result == "SQ5C5N");
        }
    }
}