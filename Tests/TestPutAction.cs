using System;
using System.Net;
using System.Net.Http;
using Moq;
using NHibernateBootstrap;
using NUnit.Framework;
using PoupeMe.Api.Controllers.Actions;
using PoupeMe.Api.Models.Fillers;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestPutAction
    {
        private Mock<IRepository<FooEntity>> _repoMock;
        private FooEntityVm _fooEntityVm;
        private FooEntity _fooEntity;
        private Mock<IEntityFiller<FooEntityVm, FooEntity>> _filler;

        [Test]
        public void PutAction_Do_Success()
        {
            //setup
            Setup();

            //excercise
            var action = new PutAction<FooEntityVm, FooEntity>(_repoMock.Object, _filler.Object);
            var responseMessage = action.Do(1, _fooEntityVm);

            //verify
            Verify(responseMessage, HttpStatusCode.OK);

        }

        [Test]
        public void PutAction_Do_WithCallback()
        {
            Setup();

            //excercise
            var action = new PutAction<FooEntityVm, FooEntity>(_repoMock.Object, _filler.Object);
            var callbackCalled = false;
            var responseMessage = action.Do(1, _fooEntityVm, e => callbackCalled = true);

            //verify
            Verify(responseMessage, HttpStatusCode.OK);
            Assert.IsTrue(callbackCalled);
        }

        [Test]
        public void PutAction_Do_ErrorInDbCommit()
        {
            Setup();
            _repoMock.Setup(m => m.Commit()).Throws<Exception>();

            //excercise
            var action = new PutAction<FooEntityVm, FooEntity>(_repoMock.Object, _filler.Object);
            var responseMessage = action.Do(1, _fooEntityVm);

            //verify
            Verify(responseMessage, HttpStatusCode.NotFound);
        }

        [Test]
        public void PutAction_Do_InvalidRequest()
        {
            Setup();

            //excercise
            var action = new PutAction<FooEntityVm, FooEntity>(_repoMock.Object, _filler.Object);
            var responseMessage = action.Do(2, _fooEntityVm);

            //verify
            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
        }


        #region Private Methods

        private void Setup()
        {
            _fooEntity = new FooEntity();
            _fooEntityVm = new FooEntityVm { Id = 1 };
            _repoMock = new Mock<IRepository<FooEntity>>();
            _repoMock.Setup(m => m.Find(1)).Returns(_fooEntity);
            _filler = new Mock<IEntityFiller<FooEntityVm, FooEntity>>();
            _filler.Setup(m => m.Fill(_fooEntityVm, _fooEntity)).Returns(_fooEntity);
        }

        private void Verify(HttpResponseMessage responseMessage, HttpStatusCode code)
        {
            _repoMock.Verify(m => m.Edit(_fooEntity));
            _repoMock.Verify(m => m.Commit());
            Assert.AreEqual(code, responseMessage.StatusCode);
        }

        #endregion
    }
}