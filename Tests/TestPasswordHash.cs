using NUnit.Framework;
using PoupeMe.Api.Components;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestPasswordHash
    {
        [Test]
        public void PasswordHash_Success()
        {
            Assert.AreEqual("f74a11a954b33f6bf744affd6dd7dc8f", "abcdef".ToPasswordHash());
        }
    }
}