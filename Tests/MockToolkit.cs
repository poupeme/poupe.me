using Moq;
using NHibernateBootstrap;
using PoupeMe.Api.Components;
using PoupeMe.Api.Models.Db;

namespace PoupeMe.Api.Tests
{
    public static class MockToolkit
    {
        public static readonly Mock<IRepository<Dealer>> DealerRepoMock = new Mock<IRepository<Dealer>>(); 
        public static readonly Mock<IHashCreator> HashCreatorMock = new Mock<IHashCreator>();
    }
}