using System.Collections.Generic;
using JetBrains.Annotations;
using NHibernateBootstrap;
using PoupeMe.Api.Models.Db;
using PoupeMe.Api.Models.Interfaces;

namespace PoupeMe.Api.Tests
{
    [UsedImplicitly]
    public class MemoryUnitOfWork : IUnitOfWork
    {
        readonly List<Dealer> _dealers = new List<Dealer>();
        readonly List<BudgetItem> _budgetItems = new List<BudgetItem>();
        readonly List<User> _users = new List<User>();
        readonly List<Transaction> _transactions = new List<Transaction>();

        public MemoryUnitOfWork(User user)
        {
            User = user;
            DealerRepository = new MemoryRepository<Dealer>(_dealers);
            BudgetItemRepository = new MemoryRepository<BudgetItem>(_budgetItems);
            UserRepository = new MemoryRepository<User>(_users);
            TransactionRepository = new MemoryRepository<Transaction>(_transactions);
        }

        public MemoryUnitOfWork(): this(null)
        {
        }

        public User User { get; private set; }
        public IRepository<Transaction> TransactionRepository { get; private set; }
        public IRepository<Dealer> AccountRepository { get; private set; }
        public IRepository<Dealer> CategoryRepository { get; private set; }
        public IRepository<Dealer> DealerRepository { get; private set; }
        public IRepository<BudgetItem> BudgetItemRepository { get; private set; }
        public IRepository<User> UserRepository { get; private set; }

        [UsedImplicitly]
        public virtual IExtraDbOperations Operations { get; set; }

        public void ImportTransactionRepository(List<Transaction> items)
        {
            TransactionRepository = new MemoryRepository<Transaction>(items);
        }
        public void ImportDealerRepository(List<Dealer> items)
        {
            DealerRepository = new MemoryRepository<Dealer>(items);
        }
        public void ImportBudgetItemRepository(List<BudgetItem> items)
        {
            BudgetItemRepository = new MemoryRepository<BudgetItem>(items);
        }
        public void ImportCategoryRepository(List<Dealer> items)
        {
            CategoryRepository = new MemoryRepository<Dealer>(items);
        }
        public void ImportAccountRepository(List<Dealer> items)
        {
            AccountRepository = new MemoryRepository<Dealer>(items);
        }

        public bool Login(int userId)
        {
            return true;
        }

        public bool Login(string userId, string password)
        {
            return true;
        }

        public void Dispose()
        {
            //do nothing
        }

        public void Commit()
        {
            //do nothing
        }
    }
}