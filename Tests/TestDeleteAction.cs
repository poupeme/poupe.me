using System;
using System.Net;
using Moq;
using NHibernateBootstrap;
using NUnit.Framework;
using PoupeMe.Api.Controllers.Actions;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestDeleteAction
    {
        private Mock<IRepository<FooEntity>> _repoMock;

        [Test]
        public void DeleteAction_Do_NotFound()
        {
            _repoMock = new Mock<IRepository<FooEntity>>();

            var action = new DeleteAction<FooEntity>(_repoMock.Object);
            var response = action.Do(1);

            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public void DeleteAction_Do_DbCommitError()
        {
            _repoMock = new Mock<IRepository<FooEntity>>();
            _repoMock.Setup(m => m.Find(1)).Returns(new FooEntity());
            _repoMock.Setup(m => m.Commit()).Throws<Exception>();

            var action = new DeleteAction<FooEntity>(_repoMock.Object);
            var response = action.Do(1);

            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            _repoMock.Verify(m => m.Commit());
        }

        [Test]
        public void DeleteAction_Do_Success()
        {
            var fooEntity = new FooEntity();
            _repoMock = new Mock<IRepository<FooEntity>>();
            _repoMock.Setup(m => m.Find(1)).Returns(fooEntity);

            var action = new DeleteAction<FooEntity>(_repoMock.Object);
            var response = action.Do(1);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            _repoMock.Verify(m => m.Remove(fooEntity));
            _repoMock.Verify(m => m.Commit());
        }
    }
}