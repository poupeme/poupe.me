using NUnit.Framework;
using PoupeMe.Api.Models.Fillers;
using PoupeMe.Api.Models.ViewModels.Transaction;

namespace PoupeMe.Api.Tests
{
    [TestFixture]
    public class TestTransactionFiller
    {
        [Test]
        public void TransactionFiller_Fill_Success()
        {
            //setup
            MockToolkit.DealerRepoMock.Setup(r => r.Find(1)).Returns(TestToolkit.DemoCategory);
            MockToolkit.DealerRepoMock.Setup(r => r.Find(2)).Returns(TestToolkit.DemoAccount);

            var filler = new TransactionFiller(MockToolkit.DealerRepoMock.Object, MockToolkit.HashCreatorMock.Object);

            //excercise
            var result = filler.Fill(TestToolkit.DemoTransactionVm);

            //verify
            AssertEx.PropertyValuesAreEquals(TestToolkit.DemoTransaction, result);
        }

        [Test]
        public void TransactionFiller_Fill_SuccessWhenIsUnhashed()
        {
            //setup
            MockToolkit.DealerRepoMock.Setup(r => r.Find(1)).Returns(TestToolkit.DemoCategory);
            MockToolkit.DealerRepoMock.Setup(r => r.Find(2)).Returns(TestToolkit.DemoAccount);
            MockToolkit.HashCreatorMock
                .Setup(h => h.Create(TestToolkit.DemoTransactionVm.Date, TestToolkit.DemoTransactionVm.Memo, TestToolkit.DemoTransactionVm.Value))
                .Returns("ABCDEF");
            var filler = new TransactionFiller(MockToolkit.DealerRepoMock.Object, MockToolkit.HashCreatorMock.Object);

            //excercise
            var result = filler.Fill(TestToolkit.DemoUnhashedTransactionVm);

            //verify
            AssertEx.PropertyValuesAreEquals(TestToolkit.DemoTransaction, result);
            MockToolkit.HashCreatorMock.Verify(h => h.Create(TestToolkit.DemoTransactionVm.Date, TestToolkit.DemoTransactionVm.Memo, TestToolkit.DemoTransactionVm.Value));
        }

        [Test]
        public void TransactionFiller_Fill_SuccessWithNullDealers()
        {
            var filler = new TransactionFiller(MockToolkit.DealerRepoMock.Object, MockToolkit.HashCreatorMock.Object);
            var result = filler.Fill(new TransactionVm());
            Assert.AreEqual(null, result.DestinyDealer);
            Assert.AreEqual(null, result.SourceDealer);
        }
    }
}